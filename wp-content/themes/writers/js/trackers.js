
/*Trackers Header*/

ga('create', 'UA-100954798-1', 'auto', 'emailHeader');
ga('create', 'UA-100954798-1', 'auto', 'twitterHeader');
ga('create', 'UA-100954798-1', 'auto', 'facebookHeader');
ga('create', 'UA-100954798-1', 'auto', 'skypeHeader');
ga('create', 'UA-100954798-1', 'auto', 'googleHeader');
ga('create', 'UA-100954798-1', 'auto', 'youtubeHeader');

ga('create', 'UA-100954798-1', 'auto', 'plataformaMenuHeader');
ga('create', 'UA-100954798-1', 'auto', 'vantagensMenuHeader');
ga('create', 'UA-100954798-1', 'auto', 'tecnologiasMenuHeader');
ga('create', 'UA-100954798-1', 'auto', 'contatoMenuHeader');
ga('create', 'UA-100954798-1', 'auto', 'blogMenuHeader');
ga('create', 'UA-100954798-1', 'auto', 'manuaisMenuHeader');

ga('create', 'UA-100954798-1', 'auto', 'bate-papoAtivar');

/*Trackers Home*/

ga('create', 'UA-100954798-1', 'auto', 'whatsappHome');
ga('create', 'UA-100954798-1', 'auto', 'telegramHome');

/*Trackers Plataforma*/

ga('create', 'UA-100954798-1', 'auto', 'previousPlataforma');
ga('create', 'UA-100954798-1', 'auto', 'nextPlataforma');

ga('create', 'UA-100954798-1', 'auto', 'atendimentosPlataforma');
ga('create', 'UA-100954798-1', 'auto', 'monitoramentoPlataforma');
ga('create', 'UA-100954798-1', 'auto', 'contatosPlataforma');
ga('create', 'UA-100954798-1', 'auto', 'personalizacaoPlataforma');
ga('create', 'UA-100954798-1', 'auto', 'campanhasPlataforma');
ga('create', 'UA-100954798-1', 'auto', 'modulosPlataforma');

ga('create', 'UA-100954798-1', 'auto', 'atendimentosSaibaMais');
ga('create', 'UA-100954798-1', 'auto', 'monitoramentoSaibaMais');
ga('create', 'UA-100954798-1', 'auto', 'contatosSaibaMais');
ga('create', 'UA-100954798-1', 'auto', 'personalizacaoSaibaMais');
ga('create', 'UA-100954798-1', 'auto', 'campanhasSaibaMais');
ga('create', 'UA-100954798-1', 'auto', 'modulosSaibaMais');

/*Trackers Interacao*/

ga('create', 'UA-100954798-1', 'auto', 'enviarOiWhatsapp');

/*Trackers Vantagens*/

ga('create', 'UA-100954798-1', 'auto', 'vantagensSaibaMais');

/*Trackers Gestao*/

ga('create', 'UA-100954798-1', 'auto', 'gestaoSaibaMais');

/*Trackers Apps*/

ga('create', 'UA-100954798-1', 'auto', 'navigateApps');

ga('create', 'UA-100954798-1', 'auto', 'downloadAtendimentoApp');
ga('create', 'UA-100954798-1', 'auto', 'downloadImportacaoApp');
ga('create', 'UA-100954798-1', 'auto', 'downloadQRApp');

/*Trackers Contato*/

ga('create', 'UA-100954798-1', 'auto', 'cPanel');
ga('create', 'UA-100954798-1', 'auto', 'sqlServer');
ga('create', 'UA-100954798-1', 'auto', 'PHP');
ga('create', 'UA-100954798-1', 'auto', 'acunetix');
ga('create', 'UA-100954798-1', 'auto', 'JAVA');
ga('create', 'UA-100954798-1', 'auto', 'angularJS');
ga('create', 'UA-100954798-1', 'auto', 'OWASP');

ga('create', 'UA-100954798-1', 'auto', 'telefoneContato');
ga('create', 'UA-100954798-1', 'auto', 'skypeContato');
ga('create', 'UA-100954798-1', 'auto', 'emailContato');

ga('create', 'UA-100954798-1', 'auto', 'sejaRepresentante');

/*Trackers Footer*/

ga('create', 'UA-100954798-1', 'auto', 'telefoneComercialFooter');
ga('create', 'UA-100954798-1', 'auto', 'emailComercialFooter');

ga('create', 'UA-100954798-1', 'auto', 'telefoneSuporteFooter');
ga('create', 'UA-100954798-1', 'auto', 'emailSuporteFooter');

ga('create', 'UA-100954798-1', 'auto', 'telefoneFinancieiroFooter');
ga('create', 'UA-100954798-1', 'auto', 'emailFinancieiroFooter');

ga('create', 'UA-100954798-1', 'auto', 'inicioMenuFooter');
ga('create', 'UA-100954798-1', 'auto', 'plataformaMenuFooter');
ga('create', 'UA-100954798-1', 'auto', 'vantagensMenuFooter');
ga('create', 'UA-100954798-1', 'auto', 'tecnologiasMenuFooter');
ga('create', 'UA-100954798-1', 'auto', 'contatoMenuFooter');
ga('create', 'UA-100954798-1', 'auto', 'blogMenuFooter');
ga('create', 'UA-100954798-1', 'auto', 'FAQMenuFooter');

ga('create', 'UA-100954798-1', 'auto', 'facebookFooter');
ga('create', 'UA-100954798-1', 'auto', 'twitterFooter');
ga('create', 'UA-100954798-1', 'auto', 'youtubeFooter');
ga('create', 'UA-100954798-1', 'auto', 'googleFooter');
ga('create', 'UA-100954798-1', 'auto', 'alertrackAppFooter');
ga('create', 'UA-100954798-1', 'auto', 'whatsappFooter');
ga('create', 'UA-100954798-1', 'auto', 'telegramFooter');

/* Trackers Scroll */

ga('create', 'UA-100954798-1', 'auto', 'plataformaScroll');
ga('create', 'UA-100954798-1', 'auto', 'interactionScroll');
ga('create', 'UA-100954798-1', 'auto', 'vantagensScroll');
ga('create', 'UA-100954798-1', 'auto', 'diferenciaisScroll');
ga('create', 'UA-100954798-1', 'auto', 'appsScroll');
ga('create', 'UA-100954798-1', 'auto', 'contatoScroll');

function isVisible($el) {
  var winTop = $(window).scrollTop();
  var winBottom = winTop + $(window).height();
  var elTop = $el.offset().top;
  var elBottom = elTop + $el.height();
	return ((elTop - 100) < winTop && (elBottom >= winBottom));
}

$(function() {
  $(window).scroll(function() {

		if(isVisible($("#plataforma"))){
      ga('plataformaScroll.send', 'event', 'plataformaScroll', 'click', 'Scroll');
		}

		if(isVisible($("#interaction"))){
      ga('interactionScroll.send', 'event', 'interactionScroll', 'click', 'Scroll');
		}

		if(isVisible($("#vantagens"))){
      ga('vantagensScroll.send', 'event', 'vantagensScroll', 'click', 'Scroll');
		}

		if(isVisible($("#diferenciais"))){
      ga('diferenciaisScroll.send', 'event', 'diferenciaisScroll', 'click', 'Scroll');
		}

		if(isVisible($("#apps"))){
      ga('appsScroll.send', 'event', 'appsScroll', 'click', 'Scroll');
		}

		if(isVisible($("#contato"))){
      ga('contatoScroll.send', 'event', 'contatoScroll', 'click', 'Scroll');
		}

  });
});
