jQuery(document).ready(function () {

    /**
     * Scroll Páginas
     */
    jQuery('.scroll, .menu-rodape .scroll').on('click', function () {
        var verify = jQuery('#hidden-menu').css('display');
        var div = jQuery(this).attr('href');

        jQuery('.scroll').removeClass('active');
        jQuery(this).addClass('active');

        if(div != '#home' && verify == 'block'){
            $('#sac-menu').toggle();
        }

        jQuery("html, body").animate({
            scrollTop: jQuery(div).offset().top
        }, 400);
    });

    /**
     * Menu
     */
    jQuery('#hidden-menu a').on('click', function () {
        $('#sac-menu').toggle();
        return false;
    });

    /**
     * Formulário de Envio
     */
    var txt_name = jQuery('input[name=mail_name]').val();
    var txt_email = jQuery('input[name=mail_email]').val();
    var txt_phone = jQuery('input[name=mail_phone]').val();
    var txt_message = jQuery('textarea[name=mail_message]').val();

    jQuery('input, textarea').on('focus', function (e) {
        var val = jQuery(this).val();
        var name = jQuery(this).attr('name');

        if (name == 'mail_name' && val == txt_name) {
            jQuery(this).val(null);
        } else if (name == 'mail_email' && val == txt_email) {
            jQuery(this).val(null);
        } else if (name == 'mail_phone' && val == txt_phone) {
            jQuery(this).val(null);
        } else if (name == 'mail_message' && val == txt_message) {
            jQuery(this).val(null);
        }
    });

    jQuery('input, textarea').on('blur', function (e) {
        var val = jQuery(this).val();
        var name = jQuery(this).attr('name');

        if (val.length <= 0) {
            if (name == 'mail_name') {
                jQuery(this).val(txt_name)
            } else if (name == 'mail_email') {
                jQuery(this).val(txt_email)
            } else if (name == 'mail_phone') {
                jQuery(this).val(txt_phone)
            } else if (name == 'mail_message') {
                jQuery(this).val(txt_message)
            }
        }
    });

    /**
     * Sliders Plataforma
     */
    var slideTo = function (to) {
        var limit = jQuery('.tab-item').length;

        var next = 0;
        var prev = 0;

        if (to == 1) {
            next = 2;
            prev = limit;
        } else if (to > 1 && to < limit) {
            next = to + 1;
            prev = to - 1;
        } else {
            next = 1;
            prev = to - 1;
        }

        jQuery('#tab-' + prev + ' .image').addClass('animated bounceOutLeft');
        jQuery('#tab-' + prev + ' .info').addClass('animated bounceOutRight');

        setTimeout(function () {
            jQuery('.tab-item .image').removeClass('animated bounceOutLeft bounceInLeft');
            jQuery('.tab-item .info').removeClass('animated bounceOutRight bounceInRight');

            jQuery('.tab,.tab-item').removeClass('active');
            jQuery('.tab-select-' + to).addClass('active');
            jQuery('#tab-' + to).addClass('active');
            jQuery('#tab-' + to + ' .image').addClass('animated bounceInLeft');
            jQuery('#tab-' + to + ' .info').addClass('animated bounceInRight');

            jQuery('.tab-ctl-right').attr('tab-to', next);
            jQuery('.tab-ctl-left').attr('tab-to', prev);
        }, 200);
    }

    jQuery('.tab-ctl-right,.tab-ctl-left,.tab').on('click', function () {
        slideTo(parseInt(jQuery(this).attr('tab-to')));
    });

    /**
     * Sliders Apps
     */
    var slideAppTo = function (to) {
        var limit = jQuery('.app-item').length;
        console.log(limit);

        var next = 0;
        var prev = 0;

        if (to == 1) {
            next = 2;
            prev = limit;
        } else if (to > 1 && to < limit) {
            next = to + 1;
            prev = to - 1;
        } else {
            next = 1;
            prev = to - 1;
        }

        jQuery('.app-item-' + prev + ' .app-item-image').addClass('animated bounceOutLeft');
        jQuery('.app-item-' + prev + ' .app-item-info').addClass('animated bounceOutRight');

        setTimeout(function () {
            jQuery('.app-item .app-item-image').removeClass('animated bounceOutLeft bounceInLeft');
            jQuery('.app-item .app-item-info').removeClass('animated bounceOutRight bounceInRight');

            jQuery('.app-item').removeClass('active');
            jQuery('.app-item-' + to).addClass('active');
            jQuery('.app-item-' + to + ' .app-item-image').addClass('animated bounceInLeft');
            jQuery('.app-item-' + to + ' .app-item-info').addClass('animated bounceInRight');

            jQuery('.app-ctl-right').attr('to', next);
            jQuery('.app-ctl-left').attr('to', prev);
        }, 200);
    }

    jQuery('.app-ctl-left,.app-ctl-right').on('click', function () {
        slideAppTo(parseInt(jQuery(this).attr('to')));
    });

    /**
     * Formulário de Contato
     */
    jQuery('form#Mail').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url : 'p/mail.php',
            type : 'POST',
            dataType : 'JSON',
            data : jQuery(this).serialize(),
            success : function (e) {
                console.log(e);
            }
        });

        return false;
    });


});
