<?php
/**
 * writers functions and definitions
 * Please browse readme.txt for credits and forking information
 *
 * @package writers
 */


if ( ! function_exists( 'writers_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */


function writers_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on writers, use a find and replace
	 * to change 'writers' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'writers', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270);
	add_image_size( 'writers-full-width', 1038, 576, true );


	function writers_register_writers_menus() {
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Top Primary Menu', 'writers' ),
			) );
	}

	add_action( 'init', 'writers_register_writers_menus' );


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'lighthouse_custom_background_args', array(
		'default-color' => 'f5f5f5',
		'default-image' => '',
		) ) );

}


endif; // writers_setup
add_action( 'after_setup_theme', 'writers_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 */
function writers_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'writers_content_width', 640 );
}
add_action( 'after_setup_theme', 'writers_content_width', 0 );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */

function writers_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'writers' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Widgets here will appear in your sidebar', 'writers' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="sidebar-headline-wrapper"><div class="widget-title-lines"></div><h4 class="widget-title">',
		'after_title'   => '</h4></div>',
		) );
}
add_action( 'widgets_init', 'writers_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function writers_scripts ( $in_footer ) {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.css',true );

	wp_enqueue_style( 'themify-icons', get_template_directory_uri().'/themify-icons/themify-icons.css',true );

	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/font-awesome/css/font-awesome.min.css',true );

	wp_enqueue_style( 'sacdigital', get_template_directory_uri().'/css/sacdigital.css',true );

	//wp_enqueue_style( 'kbe', get_template_directory_uri().'/css/kbe.css',true );

	wp_enqueue_script('jquery-3.2.1', get_template_directory_uri().'/js/jquery-3.2.1.js',true );

	wp_enqueue_script('all', get_template_directory_uri().'/js/all.js',true );

	if ( is_page('Home') ) {

		wp_enqueue_script('trackers', get_template_directory_uri().'/js/trackers.js',true );
		wp_enqueue_script('slider', get_template_directory_uri().'/js/slider.js',true );
		wp_enqueue_style( 'cssslider', get_template_directory_uri().'/csslider-master/build/csslider.css',true );
		wp_enqueue_style( 'devices.min', get_template_directory_uri().'/devices.css-master/assets/devices.min.css',true );
		wp_enqueue_style( 'animate', get_template_directory_uri().'/css/animate.css',true );
		wp_enqueue_style( 'arrow', get_template_directory_uri().'/css/arrow.css',true );
		wp_enqueue_style( 'chat', get_template_directory_uri().'/css/chat.css',true );
		wp_enqueue_style( 'services', get_template_directory_uri().'/css/services.css',true );
	}

	$modules = array('modulos', 'atendimento-operacional');

	if(is_page($modules)){
		wp_enqueue_style( 'header-modulo', get_template_directory_uri().'/css/header-modulo.css',true );

		if(is_page('modulos')){
			wp_enqueue_style( 'modules', get_template_directory_uri().'/css/modulos/modules.css',true );
		}

		if(is_page('atendimento-operacional')){
			wp_enqueue_style( 'atendimento', get_template_directory_uri().'/css/modulos/atendimento.css',true );
		}

	}

	if(is_page('faq')){
		wp_enqueue_style( 'faq', get_template_directory_uri().'/css/faq.css',true );
	}

	wp_enqueue_script('converters', get_template_directory_uri().'/js/converters.js',true );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js',array('jquery'),'',true);

	wp_enqueue_script( 'writers-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array('jquery'), '20130115', true );

	wp_enqueue_script( 'html5shiv', get_template_directory_uri().'/js/html5shiv.js', array(),'3.7.3',false );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}

add_action( 'wp_enqueue_scripts', 'writers_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 * Load custom nav walker
 */
if(!class_exists('wp_bootstrap_navwalker')){
	require get_template_directory() . '/inc/navwalker.php';
}


function writers_google_fonts() {
	$query_args = array(

		'family' => 'Merriweather:700,700i|Lato:400,400italic,600'
		);
	wp_register_style( 'writersgooglefonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
	wp_enqueue_style( 'writersgooglefonts');
}

add_action('wp_enqueue_scripts', 'writers_google_fonts');


function writers_new_excerpt_more( $more ) {
	if ( is_admin() ) {return $more;}$link = sprintf( '',esc_url( get_permalink( get_the_ID() ) ));
	return ' &hellip; ' . $link;

}
add_filter( 'excerpt_more', 'writers_new_excerpt_more' );




/**
*
* Custom Logo in the top menu
*
**/

function writers_logo() {
	add_theme_support('custom-logo', array(
		'size' => 'writers-logo',
		'width'                  => 250,
		'height'                 => 50,
		'flex-height'            => true,
		));
}

add_action('after_setup_theme', 'writers_logo');


/**
*
* New Footer Widgets
*
**/

function writers_footer_widget_left_init() {

	register_sidebar( array(
		'name' => esc_html__('Footer Widget left', 'writers'),
		'id' => 'footer_widget_left',
		'description'   => esc_html__( 'Widgets here will appear in your footer', 'writers' ),
		'before_widget' => '<div class="footer-widgets">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'writers_footer_widget_left_init' );

function writers_footer_widget_middle_init() {

	register_sidebar( array(
		'name' => esc_html__('Footer Widget middle', 'writers'),
		'id' => 'footer_widget_middle',
		'description'   => esc_html__( 'Widgets here will appear in your footer', 'writers' ),
		'before_widget' => '<div class="footer-widgets">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'writers_footer_widget_middle_init' );

function writers_footer_widget_right_init() {

	register_sidebar( array(
		'name' => esc_html__('Footer Widget right', 'writers'),
		'id' => 'footer_widget_right',
		'before_widget' => '<div class="footer-widgets">',
		'description'   => esc_html__( 'Widgets here will appear in your footer', 'writers' ),
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'writers_footer_widget_right_init' );



/**
*
* Admin styles
*
**/
function writers_load_custom_wp_admin_style( $hook ) {
	if ( 'appearance_page_about-writers' !== $hook ) {
		return;
	}
	wp_enqueue_style( 'writers-custom-admin-css', get_template_directory_uri() . '/css/admin.css', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'writers_load_custom_wp_admin_style' );


/*TGA activation*/

require_once get_template_directory() . '/lib/class-tgm-plugin-activation.php';

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Free Seo Optimized Responsive Theme for publication on WordPress.org
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once get_template_directory() . '/lib/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'writers_register_required_plugins' );

function writers_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'Autoptimize',
			'slug'      => 'autoptimize',
			'required'  => false,
			),

		array(
			'name'      => 'WP Super Cache',
			'slug'      => 'wp-super-cache',
			'required'  => false,
			),

		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
			),

		);

	$config = array(
		'id'           => 'writers',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.


		);

	tgmpa( $plugins, $config );
}

function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );
