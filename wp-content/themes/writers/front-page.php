<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please browse readme.txt for credits and forking information
 * @package writers
 */

get_header(); ?>

<div id="home">
	<div id="sac-top">
		 <h1>A plataforma mais completa para sua empresa</h1>
		 <h3>Somos a única plataforma do mercado que ajuda você empreender seus clientes</h3>

		 <div class="call-buttons">
				 <a class="button-wpp" href="whatsapp://send?text=Oi&phone=+5527981860090" title="+5527981860090 via Whatsapp" target="_blank" onclick="ga('whatsappHome.send', 'event', 'whatsappHome', 'click', 'Home')"><i class="fa fa-whatsapp"></i> Whatsapp</a>
				 <a class="button-tg" href="tg://resolve?domain=sacdigitalbr" title="+5527981860090 via Telegram" target="_blank" onclick="ga('telegramHome.send', 'event', 'telegramHome', 'click', 'Home')"><i class="fa fa-telegram"></i> Telegram</a>
		 </div>
 </div>
 <div class="arrow bounce"><i class="fa fa-angle-down fa-5x" aria-hidden="true"></i></div>
</div>

  <!--A Plataforma-->
  <div id="plataforma" class="sac-section">

          <div class="row hd text-center">
              <h1>O que é a SAC DIGITAL?</h1>
              <h3 class="mt30">Somos uma plataforma de SAC automatizado que ajuda empresas a atender as necessidades de seus clientes de forma organizada e flexível, com suporte a WhatsApp, Telegram, SMS e diversas ferramentas. Esta plataforma já é a melhor do MUNDO!</h3>
          </div>

          <div class="row mt30">
              <div class="tab-itens">
                  <div class="tab active tab-select-1" tab-to="1" onclick="ga('atendimentosPlataforma.send', 'event', 'atendimentosPlataforma', 'click', 'Plataforma')"><i class="ti-headphone-alt"></i> <span>Atendimentos</span></div>
                  <div class="tab tab-select-2" tab-to="2" onclick="ga('monitoramentoPlataforma.send', 'event', 'monitoramentoPlataforma', 'click', 'Plataforma')"><i class="ti-eye"></i> <span>Monitoramento</span></div>
                  <div class="tab tab-select-3" tab-to="3" onclick="ga('contatosPlataforma.send', 'event', 'contatosPlataforma', 'click', 'Plataforma')"><i class="ti-user"></i> <span>Contatos</span></div>
                  <div class="tab tab-select-4" tab-to="4" onclick="ga('personalizacaoPlataforma.send', 'event', 'personalizacaoPlataforma', 'click', 'Plataforma')"><i class="ti-paint-roller"></i> <span>Personalização</span></div>
                  <div class="tab tab-select-5" tab-to="5" onclick="ga('campanhasPlataforma.send', 'event', 'campanhasPlataforma', 'click', 'Plataforma')"><i class="ti-announcement"></i> <span>Campanhas</span></div>
                  <div class="tab tab-select-6" tab-to="6" onclick="ga('modulosPlataforma.send', 'event', 'modulosPlataforma', 'click', 'Plataforma')"><i class="ti-ticket"></i> <span>Módulos</span></div>
              </div>

              <div class="tab-slider">
                  <span class="tab-ctl-left" tab-to="6" onclick="ga('previousPlataforma.send', 'event', 'previousPlataforma', 'click', 'Plataforma')"></span>
                  <div id="tab-1" class="tab-item active">

                      <div class="image">
                          <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/item-atendimentos.jpg" alt="Gestão de Atendimentos" style="margin-top: 5px">
                      </div>

                      <div class="info">
                          <h1>Gestão de Atendimentos</h1>

                          <p><b>Seus clientes possuem a capacidade de gerenciar múltiplos canais de comunicação de forma simples, rápida e organizada. Com suporte a Whatsapp, Telegram, SMS e E-mail.</b></p>
                          <p>Oferecendo atendimentos simultâneos com um ou inúmeros usuários em um atendimento operacional por departamentos. Conquiste o máximo em autonomia, ofereça aos seus clientes um pré Atendimento Automatizado Exclusivo e Inovador.</p>

                          <ul>
                              <li><span>1</span> Gerenciamento de Protocolos de Atendimentos</li>
                              <li><span>2</span> Identificação de Contatos</li>
                              <li><span>3</span> Centralização de toda comunicação em tempo real</li>
                          </ul>

                          <a href="#" onclick="ga('atendimentosSaibaMais.send', 'event', 'atendimentosSaibaMais', 'click', 'Plataforma')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                      </div>
                  </div>
                  <div id="tab-2" class="tab-item">

                      <div class="image">
                          <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/item-monitoramento.jpg" alt="Monitoramento de Operadores" style="margin-top: 5px">
                      </div>

                      <div class="info">
                          <h1>Monitoramento Diário</h1>

                          <p><b>Seus clientes possuem a capacidade de gerenciar múltiplos canais de comunicação de forma simples, rápida e organizada. Com suporte a Whatsapp, Telegram, SMS e E-mail.</b></p>
                          <p>Oferecendo atendimentos simultâneos com um ou inúmeros usuários em um atendimento operacional por departamentos. Conquiste o máximo em autonomia, ofereça aos seus clientes um pré Atendimento Automatizado Exclusivo e Inovador.</p>

                          <ul>
                              <li><span>1</span> Ranking e Controle de Escalas dos Operadores</li>
                              <li><span>2</span> Relatórios em Tempo Real de Atendimentos</li>
                              <li><span>3</span> Monitoramento de conversação entre usuários</li>
                          </ul>

                          <a href="#" onclick="ga('monitoramentoSaibaMais.send', 'event', 'monitoramentoSaibaMais', 'click', 'Plataforma')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                      </div>
                  </div>
                  <div id="tab-3" class="tab-item">

                      <div class="image">
                          <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/item-contatos.jpg" alt="Gestão de Contatos" style="margin-top: 5px">
                      </div>

                      <div class="info">
                          <h1>Gestão de Contatos</h1>

                          <p><b>Seus clientes possuem a capacidade de gerenciar múltiplos canais de comunicação de forma simples, rápida e organizada. Com suporte a Whatsapp, Telegram, SMS e E-mail.</b></p>
                          <p>Oferecendo atendimentos simultâneos com um ou inúmeros usuários em um atendimento operacional por departamentos. Conquiste o máximo em autonomia, ofereça aos seus clientes um pré Atendimento Automatizado Exclusivo e Inovador.</p>

                          <ul>
                              <li><span>1</span> Identificação Social do Contato</li>
                              <li><span>2</span> Relatórios e Histórico de Atendimentos</li>
                              <li><span>3</span> Geolocalização e Redes Sociais</li>
                          </ul>

                          <a href="#" onclick="ga('contatosSaibaMais.send', 'event', 'contatosSaibaMais', 'click', 'Plataforma')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                      </div>
                  </div>
                  <div id="tab-4" class="tab-item">

                      <div class="image">
                          <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/item-personaliza.jpg" alt="Personalização" style="margin-top: 5px">
                      </div>

                      <div class="info">
                          <h1>Personalize sua Plataforma</h1>

                          <p><b>Seus clientes possuem a capacidade de gerenciar múltiplos canais de comunicação de forma simples, rápida e organizada. Com suporte a Whatsapp, Telegram, SMS e E-mail.</b></p>
                          <p>Oferecendo atendimentos simultâneos com um ou inúmeros usuários em um atendimento operacional por departamentos. Conquiste o máximo em autonomia, ofereça aos seus clientes um pré Atendimento Automatizado Exclusivo e Inovador.</p>

                          <ul>
                              <li><span>1</span> Títulos, logomarcas e ícones</li>
                              <li><span>2</span> Planos de fundo personalizados</li>
                              <li><span>3</span> Diversos temas para seu painel administrativo</li>
                          </ul>

                          <a href="#" onclick="ga('personalizacaoSaibaMais.send', 'event', 'personalizacaoSaibaMais', 'click', 'Plataforma')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                      </div>
                  </div>
                  <div id="tab-5" class="tab-item">

                      <div class="image">
                          <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/item-campanhas.jpg" alt="Campanhas" style="margin-top: 5px">
                      </div>

                      <div class="info">
                          <h1>Campanhas Personalizadas</h1>

                          <p><b>Seus clientes possuem a capacidade de gerenciar múltiplos canais de comunicação de forma simples, rápida e organizada. Com suporte a Whatsapp, Telegram, SMS e E-mail.</b></p>
                          <p>Oferecendo atendimentos simultâneos com um ou inúmeros usuários em um atendimento operacional por departamentos. Conquiste o máximo em autonomia, ofereça aos seus clientes um pré Atendimento Automatizado Exclusivo e Inovador.</p>

                          <ul>
                              <li><span>1</span> Campanhas segmentadas</li>
                              <li><span>2</span> Envio de Mídias e TAGS personalizadas</li>
                              <li><span>3</span> Envio em massa com relatórios analíticos e sintéticos</li>
                          </ul>

                          <a href="#" onclick="ga('campanhasSaibaMais.send', 'event', 'campanhasSaibaMais', 'click', 'Plataforma')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                      </div>
                  </div>
                  <div id="tab-6" class="tab-item">

                      <div class="image">
                          <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/item-modulos.jpg" alt="Módulos Diversos" style="margin-top: 5px">
                      </div>

                      <div class="info">
                          <h1>Integre diversos Módulos</h1>

                          <p><b>Seus clientes possuem a capacidade de gerenciar múltiplos canais de comunicação de forma simples, rápida e organizada. Com suporte a Whatsapp, Telegram, SMS e E-mail.</b></p>
                          <p>Oferecendo atendimentos simultâneos com um ou inúmeros usuários em um atendimento operacional por departamentos. Conquiste o máximo em autonomia, ofereça aos seus clientes um pré Atendimento Automatizado Exclusivo e Inovador.</p>

                          <ul>
                              <li><span>1</span> Gerenciamento de Protocolos de Atendimentos</li>
                          </ul>

                          <a href="#" onclick="ga('modulosSaibaMais.send', 'event', 'modulosSaibaMais', 'click', 'Plataforma')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                      </div>
                  </div>
                  <span class="tab-ctl-right" tab-to="2" onclick="ga('nextPlataforma.send', 'event', 'nextPlataforma', 'click', 'Plataforma')"></span>
              </div>
          </div>
  </div>

	<div id="interaction" class="sac-section">
		<div class="row hd text-center">
				<h1>Experimente a nossa plataforma</h1>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="marvel-device iphone5s silver">
				    <div class="top-bar"></div>
				    <div class="sleep"></div>
				    <div class="volume"></div>
				    <div class="camera"></div>
				    <div class="sensor"></div>
				    <div class="speaker"></div>
				    <div class="screen">
				    </div>
				    <div class="home"></div>
				    <div class="bottom-bar"></div>
				</div>
			</div>
			<div class="col-md-6" style="">

				<div class="speech-wrapper">

					<div class="bubble">
				    <div class="txt">
				      <p class="name"> SAC Digital</p>
				      <p class="message">Quer testar nossa plataforma?</p>
				      <span class="timestamp">10:21 am</span>
				    </div>
				    <div class="bubble-arrow"></div>
				  </div>

					<div class="bubble alt">
				    <div class="txt">
				      <p class="name alt">Você</p>
				      <p class="message">Quero testar agora mesmo</p>
				      <span class="timestamp">10:22 am</span>
				    </div>
				    <div class="bubble-arrow alt"></div>
				  </div>

				  <div class="bubble">
				    <div class="txt">
				      <p class="name"> SAC Digital</p>
				      <p class="message">Envie um "OI" para (27) 98186-0090</p>
				      <span class="timestamp">10:21 am</span>
				    </div>
				    <div class="bubble-arrow"></div>
				  </div>

					<div class="bubble">
				    <div class="txt">
				      <p class="name"> SAC Digital</p>
							<p class="message">Conheça nossa plataforma de atendimento em tempo real</p>
				      <span class="timestamp">10:21 am</span>
				    </div>
				    <div class="bubble-arrow"></div>
				  </div>

				  <!--  Speech Bubble alternative -->
				  <div class="bubble alt">
				    <div class="txt">
				      <p class="name alt">Você</p>
				      <p class="message">Vou testar, sim!</p>
				      <span class="timestamp">10:22 am</span>
				    </div>
				    <div class="bubble-arrow alt"></div>
				  </div>

				</div>

				<a id="whatsapp" href="whatsapp://send?text=Oi&phone=+5527981860090" target="_blank" title="Whatsapp" onclick="ga('enviarOiWhatsapp.send', 'event', 'enviarOiWhatsapp', 'click', 'Plataforma')">
					<i class="fa fa-whatsapp fa-3x" aria-hidden="true"></i>
					<h2>Enviar um "Oi"</h2>
				</a>

			</div>
		</div>
	</div>

  <div id="vantagens" class="sac-vantagens sac-section">
          <div class="row hd text-center">
              <h1>Por que ter SAC DIGITAL?</h1>
          </div>
					<div class="cntnr">
				    <div class="row">
				        <div class="col-md-3 col-sm-6">
				            <div class="serviceBox">
				                <div class="service-icon">
				                    <i class="ti-medall-alt"></i>
				                </div>
				                <h3 class="title">Inovação</h3>
				                <p class="description">
				                    Conte com o que há de melhor em Tecnologia
				                </p>
				            </div>
				        </div>
				        <div class="col-md-3 col-sm-6">
				            <div class="serviceBox">
				                <div class="service-icon">
				                    <i class="ti-face-smile"></i>
				                </div>
				                <h3 class="title">Satisfação</h3>
				                <p class="description">
				                    Atinja o máximo de satisfação para sua empresa
				                </p>
				            </div>
				        </div>
								<div class="col-md-3 col-sm-6">
				            <div class="serviceBox">
				                <div class="service-icon">
				                    <i class="ti-pie-chart"></i>
				                </div>
				                <h3 class="title">Informação</h3>
				                <p class="description">
				                    Relatórios detalhados e super exclusivos
				                </p>
				            </div>
				        </div>
				        <div class="col-md-3 col-sm-6">
				            <div class="serviceBox">
				                <div class="service-icon">
				                    <i class="ti-user"></i>
				                </div>
				                <h3 class="title">Atendimento</h3>
				                <p class="description">
				                    Atenda clientes de pequeno, médio e grande porte
				                </p>
				            </div>
				        </div>
				    </div>
						<div class="row">
							<div class="col-md-3 col-sm-6 col-md-offset-1">
									<div class="serviceBox">
											<div class="service-icon">
													<i class="ti-world"></i>
											</div>
											<h3 class="title">Escalabilidade</h3>
											<p class="description">
													Utilize zonas universais e suporte a diversos idiomas
											</p>
									</div>
							</div>
							<div class="col-md-3 col-sm-6">
									<div class="serviceBox">
											<div class="service-icon">
													<i class="ti-bell"></i>
											</div>
											<h3 class="title">Alertas</h3>
											<p class="description">
													Notifique seus clientes e emita alertas
											</p>
									</div>
							</div>
							<div class="col-md-3 col-sm-6 automacao">
									<div class="serviceBox">
											<div class="service-icon">
													<i class="fa fa-bullhorn"></i>
											</div>
											<h3 class="title">Automação</h3>
											<p class="description">
													Campanhas imediatas, agendadas e recorrentes
											</p>
									</div>
							</div>
						</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="v-info mt20">
							<a href="#" onclick="ga('vantagensSaibaMais.send', 'event', 'vantagensSaibaMais', 'click', 'Vantagens')">Saiba Mais <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
						</div>
					</div>
				</div>
  </div>

  <!--Diferenciais-->
  <div id="diferenciais" class="sac-section pt10">
          <div class="row hd text-center mb20">
              <h1>Gestão e Enriquecimento</h1>
              <h3>Unimos a gestão dos seus atendimentos e o enriquecimento de todos os contatos</h3>
          </div>

          <div class="row d-group mt30">
              <div class="d-list">
                  <div class="d-icon">
                      <i class="ti-target"></i>
                  </div>
                  <div class="d-title">Enriquecimento de Leads</div>
                  <div class="d-info">Saiba tudo sobre seus contatos, links sociais, perfil socio econômico, geolocalização e muito mais.</div>
              </div>

              <div class="d-list">
                  <div class="d-icon">
                      <i class="ti-comments"></i>
                  </div>
                  <div class="d-title">Comunicação Colaborativa</div>
                  <div class="d-info">Comunicação em tempo real entre colaboradores com diversos níveis de acesso, contando ainda com tickets.</div>
              </div>

              <div class="d-list">
                  <div class="d-icon">
                      <i class="ti-blackboard"></i>
                  </div>
                  <div class="d-title">Facilidade</div>
                  <div class="d-info">Personalizar e criar regras de automatização do fluxo de auto atendimento nunca foi tão fácil.</div>
              </div>

              <div class="d-list">
                  <div class="d-icon">
                      <i class="ti-comments-smiley"></i>
                  </div>
                  <div class="d-title">Inteligência Artificial</div>
                  <div class="d-info">A única plataforma que cresce a cada interação, contando com atualizações e lançamentos constantes de módulos.</div>
              </div>
          </div>
          <div class="row" style="text-align: center;">
              <a href="#" class="show_more" title="Módulos do Sistema" onclick="ga('gestaoSaibaMais.send', 'event', 'gestaoSaibaMais', 'click', 'Gestao')">Conheça mais sobre nossa plataforma <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
          </div>
  </div>

	<div id="apps" class="sac-section">
		<div class="row hd text-center">
			<h1>Aplicativos SAC Digital</h1>
		</div>
		<div id="phone">

				<div class="marvel-device nexus5">
    			<div class="top-bar"></div>
    			<div class="sleep"></div>
    			<div class="volume"></div>
    			<div class="camera"></div>
    			<div class="screen csslider infinity inside">

				        <input type="radio" name="slides" id="slides_1" checked />
				        <input type="radio" name="slides" id="slides_2" />
				        <input type="radio" name="slides" id="slides_3" />
				        <ul>
				            <li>
				                <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/app_chat_tela.png" />
				            </li>
				            <li>
				                <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/app_importacao_tela.png" />
				            </li>
				            <li>
				                <img src="http://localhost/sacdigital/wp-content/uploads/2017/11/app_leitor-QR_tela.png" />
				            </li>
				        </ul>
				        <div class="arrows">
				            <label for="slides_1" onclick="changeSlide(1);ga('navigateApps.send', 'event', 'navigateApps', 'click', 'Apps');"></label>
				            <label for="slides_2" onclick="changeSlide(2);ga('navigateApps.send', 'event', 'navigateApps', 'click', 'Apps');"></label>
				            <label for="slides_3" onclick="changeSlide(3);ga('navigateApps.send', 'event', 'navigateApps', 'click', 'Apps');"></label>
				        </div>
    		</div>
			</div>

			<div class="app-item-info" id="ai1">
					<h1>Atendimento Operacional</h1>
					<p>Disponibilizamos ainda aplicativo exclusivo para gestão dos atendimentos de seus operadores, monitores ou gestores, com toda a mobilidade de comunicação que você precisa.</p>
					<a href="https://play.google.com/store/apps/details?id=project.android.sacdigital" target="_blank" title="APP SACDIGITAL" onclick="ga('downloadAtendimentoApp.send', 'event', 'downloadAtendimentoApp', 'click', 'Apps')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/gplay.png" alt="APP SACDIGITAL"></a>
			</div>
			<div class="app-item-info" id="ai2" style="display:none;">
					<h1>Importação de Contatos</h1>
					<p>NÃO DEIXE NINGUÉM PARA TRÁS! <br> Adicione todos contatos da sua agenda para a melhor plataforma de gestão de atendimentos, de forma rápida, simples e gratuita.</p>
					<a href="https://play.google.com/store/apps/details?id=sac.digital.importacao" target="_blank" title="APP SACDIGITAL Importação" onclick="ga('downloadImportacaoApp.send', 'event', 'downloadImportacaoApp', 'click', 'Apps')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/gplay.png" alt="APP SACDIGITAL Importação"></a>
			</div>
			<div class="app-item-info" id="ai3" style="display:none;">
					<h1>QRCode Ideal</h1>
					<p>Disponibilizamos ainda aplicativo utilitário para leitura de QRCode, super completo, gratuito e sem propagandas.</p>
					<a href="https://play.google.com/store/apps/details?id=projeto.alertrack.qrcodeideal" target="_blank" title="QRCode Ideal" onclick="ga('downloadQRApp.send', 'event', 'downloadQRApp', 'click', 'Apps')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/gplay.png" alt="QRCode Ideal"></a>
			</div>
		</div>
	</div>

	<!--Contato-->
	<div id="contato" class="sac-section" style="padding-bottom:0px !important;">

		<div id="tecnologias" class="hide_xs">
						-<div class="row text-center">
								<h1>Tecnologias</h1>
						</div>

						<div class="row list-tecs">
								<a href="https://cpanel.com/products/" title="WHM & CPANEL" target="_blank" onclick="ga('cPanel.send', 'event', 'cPanel', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_cpanel.jpg" alt=""></a>
								<a href="https://www.microsoft.com/pt-br/sql-server/" title="SQL Server" target="_blank" onclick="ga('sqlServer.send', 'event', 'sqlServer', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_sql.jpg" alt=""></a>
								<a href="https://secure.php.net/" title="PHP" target="_blank" onclick="ga('PHP.send', 'event', 'PHP', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_php.jpg" alt=""></a>
								<a href="https://www.acunetix.com/" title="Acunetix" target="_blank" onclick="ga('acunetix.send', 'event', 'acunetix', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_acunetix.jpg" alt=""></a>
								<a href="https://www.java.com/" title="JAVA" target="_blank" onclick="ga('JAVA.send', 'event', 'JAVA', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_java.jpg" alt=""></a>
								<a href="https://angularjs.org/" title="Angular JS" target="_blank" onclick="ga('angularJS.send', 'event', 'angularJS', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_angular.jpg" alt=""></a>
								<a href="https://www.owasp.org" title="OWASP" target="_blank" onclick="ga('OWASP.send', 'event', 'OWASP', 'click', 'Tecnologias')"><img src="http://localhost/sacdigital/wp-content/uploads/2017/11/tec_owasp.jpg" alt=""></a>
						</div>
		</div>

	    <div id="suporte">
	        <div class="row container">
						<div class="col-md-6 suporte-text">
							<div class="s-left">
	                <div class="s-info">
	                    <h1>Suporte disponível para te atender</h1>
	                    <p>Queremos que você tenha a melhor experiência de atendimento, até quando estiver com problemas ou dúvidas!</p>
	                </div>
	                <div class="s-buttons">
	                    <a href="tel:+552732110011" class="phone" onclick="ga('telefoneContato.send', 'event', 'telefoneContato', 'click', 'Contato')"><i class="ti-mobile"></i> ( 27 ) 3211 - 0011</a>
											<a href="skype:sacdigitalbr?call" class="skype" onclick="ga('skypeContato.send', 'event', 'skypeContato', 'click', 'Contato')"><i class="fa fa-skype"></i> sacdigitalbr</a>
	                    <a href="mailto:atendimento@sac.digital" class="email" onclick="ga('emailContato.send', 'event', 'emailContato', 'click', 'Contato')"><i class="ti-email"></i> atendimento@sac.digital</a>
	                </div>
	            </div>
	            <div class="s-right hide_xs hide_xmd hide_tablet">
	                <h1>Seja um Representante</h1>
	                <p>Conheça os recursos da SAC DIGITAL e descubra como podemos levar sua empresa para um próximo nível.</p>
	                <a href="#" onclick="ga('sejaRepresentante.send', 'event', 'sejaRepresentante', 'click', 'Contato')">Seja um Representante</a>
	            </div>
							<div class="mas-info" style="float: left;margin-top: 30px;">
								<div class="r-phone-icon"><i class="fa fa-phone-square" style="color: white;"></i></div>
								<div class="r-suport">
                    <p style="margin-bottom: 0px;"><b style="color:white;font-style:italic;">Precisa de mais informações?</b> <span style="text-align: right;color:white;">( 27 ) 99828 - 2828 <br>24hrs</span></p>
                    <p style="color:white;">Fale com um de nossos consultores</p>
                </div>
							</div>
						</div>
						<div class="col-md-6 formulario">
							<?php echo do_shortcode( '[contact-form-7 id="36" title="Contact form 1" html_id="mail"]' ); ?>
						</div>
	        </div>
	    </div>

</div><!--.container-->
<?php get_footer(); ?>
