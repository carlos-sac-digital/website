<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
* Please browse readme.txt for credits and forking information
 * @package writers
 */

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width" />
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-100954798-1', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div id="page" class="hfeed site">
    <header id="masthead">

      <div id="sac-top-header">
          <!--Nav Topo-->
          <div id="sac-top-nav">
              <div class="container">
                  <div class="tab-content">
                      <div class="tab-left">
                          <div class="sac-list-group-item hide_xs"><a href="mailto:atendimento@sac.digital" onclick="ga('emailHeader.send', 'event', 'emailHeader', 'click', 'Header')"><i class="ti-email"></i> atendimento@sac.digital</a></div>
                          <div class="sac-list-group-item hide_xmd hide_tablet"><a href="tel:+552732110011"><i class="fa fa-phone"></i> (27) 3211-0011</a></div>
                          <div class="sac-list-group-item hide_xtablet hide_xs hide_xmd hide_tablet"><a href="tel:+5527998282828" ><i class="fa fa-whatsapp"></i> (27) 99828-2828</a></div>
                          <div class="sac-list-group-item hide_xtablet hide_xs hide_xmd hide_tablet hidden-md"><a href="tel:+5527981843007"><i class="fa fa-whatsapp"></i> (27) 98184-3007</a></div>
                      </div>

                      <div class="tab-right">
                          <div class="sac-list-group-social"><a href="https://www.facebook.com/sacdigitalbr/" title="Facebook" target="_blank" onclick="ga('facebookHeader.send', 'event', 'facebookHeader', 'click', 'Header')"><i class="fa fa-facebook"></i></a></div>
                          <div class="sac-list-group-social"><a href="https://twitter.com/sacdigitalbr" title="Twitter" target="_blank" onclick="ga('twitterHeader.send', 'event', 'twitterHeader', 'click', 'Header')"><i class="fa fa-twitter"></i></a></div>
                          <div class="sac-list-group-social hide_xtablet hide_xs hide_xmd"><a href="skype:sacdigitalbr?call" title="Skype" target="_blank" onclick="ga('skypeHeader.send', 'event', 'skypeHeader', 'click', 'Header')"><i class="fa fa-skype"></i></a></div>
                          <div class="sac-list-group-social hide_xtablet hide_xs hide_xmd"><a href="https://plus.google.com/+sacdigitalbr" title="Google+" target="_blank" onclick="ga('googleHeader.send', 'event', 'googleHeader', 'click', 'Header')"><i class="fa fa-google-plus"></i></a></div>
                          <div class="sac-list-group-social hide_xtablet hide_xs hide_xmd"><a href="https://www.youtube.com/channel/UCXSjILPCsSzJi0DkfJEtqwQ" title="Youtube" target="_blank" onclick="ga('youtubeHeader.send', 'event', 'youtubeHeader', 'click', 'Header')"><i class="fa fa-youtube-play"></i></a></div>
                      </div>
                  </div>
              </div>
          </div>

          <div id="sac-nav" class="white">
              <div id="sac-nav-full" class="container">
                  <div id="sac-logo">
                    <a href="http://sac.digital">
                      <svg id="Group_1" data-name="Group 1" xmlns="http://www.w3.org/2000/svg" viewBox="365.398 329.744 386.58 400.054">
                        <defs>
                          <style>
                            .cls-1 {fill: #204a68;}
                            .cls-2 {fill: #3f75a4;}
                            .cls-3 {fill: #6298d1;}
                          </style>
                        </defs>
                        <path id="Path_1" data-name="Path 1" class="cls-1" d="M416.4,351.1l199.156-59.357,186.583,72.014L603.262,423.641Z" transform="translate(-51 38)"/>
                        <path id="Path_2" data-name="Path 2" class="cls-2" d="M551.423,461.539l.789,53.378L401.905,457.888v52.329l150.308,58.137.222,161.444L364.649,658.43V604.084l149.565,56.874V608.813l-149.565-57.5V388.44Z" transform="translate(1)"/>
                        <path id="Path_3" data-name="Path 3" class="cls-3" d="M751.137,455.524v-53.7l-198.794,59.7.821,53.394Z"/>
                        <path id="Path_4" data-name="Path 4" class="cls-3" d="M480.283,487.024l-77.4,23.152-.29-52.546Z"/>
                        <path id="Path_5" data-name="Path 5" class="cls-1" d="M674.587,478.511c1,.3,77.391,30.552,77.391,30.552l-198.91,59.3-150.26-58.15,77.051-23.278,73.209,27.825Z"/>
                        <path id="Path_6" data-name="Path 6" class="cls-3" d="M751.83,669.719l-198.4,60.06-.266-161.491L751.83,509.039Z"/>
                        <path id="Path_7" data-name="Path 7" class="cls-1" d="M441.461,580.427l-75.783,23.784L515.227,661V608.843Z"/>
                      </svg>
                    </a>
                  </div>

                  <div id="hidden-menu">
                      <a href="#"><i class="fa fa-bars"></i></a>
                  </div>
                  <div id="sac-menu">
                      <ul>
                          <li><a class="scroll" href="#plataforma" title="Plataforma" onclick="ga('plataformaMenuHeader.send', 'event', 'plataformaMenuHeader', 'click', 'Header')">Plataforma</a></li>
                          <li><a class="scroll" href="#vantagens" title="Vantagens" onclick="ga('vantagensMenuHeader.send', 'event', 'vantagensMenuHeader', 'click', 'Header')">Vantagens</a></li>
                          <li><a class="scroll" href="#tecnologias" title="Tecnologias" onclick="ga('tecnologiasMenuHeader.send', 'event', 'tecnologiasMenuHeader', 'click', 'Header')">Tecnologias</a></li>
                          <li><a class="scroll" href="#suporte" title="Contato" onclick="ga('contatoMenuHeader.send', 'event', 'contatoMenuHeader', 'click', 'Header')">Contato</a></li>
                          <li><a href="#" title="Blog" onclick="ga('blogMenuHeader.send', 'event', 'blogMenuHeader', 'click', 'Header')">Blog</a></li>
                          <li><a href="#" title="FAQ" onclick="ga('manuaisMenuHeader.send', 'event', 'manuaisMenuHeader', 'click', 'Header')">Manuais</a></li>
                          <li class="bPH"><a href="#" title="Bate Papo" class="bate-papo scx-chat-btn" onclick="ga('bate-papoAtivar.send', 'event', 'batePapoAtivar', 'click', 'Header')"><i class="ti-headphone-alt" style="margin-right:5px;"></i>Bate Papo</a></li>
                          <li class="focus"><a href="tel:+5527998282828" title="FAQ" class="test"><i class="ti-mobile"></i> Ligue agora ( 27 ) 99828 - 2828</a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>

      <nav class="navbar lh-nav-bg-transform navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container" id="navigation_menu">
          <div class="navbar-header">
            <?php if ( has_nav_menu( 'primary' ) ) { ?>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only"><?php echo esc_html('Toggle Navigation', 'writers') ?></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <?php } ?>
          </div>
          </div><!--#container-->
        </nav>

        <?php if ( is_front_page() ) { ?>
        <div class="site-header">
          <div class="site-branding">
            <span class="home-link">
              <?php if (get_theme_mod('hero_image_title') ) : ?>
              <span class="frontpage-site-title"><?php echo wp_kses_post(get_theme_mod('hero_image_title')) ?></span>
            <?php endif; ?>

            <?php if (get_theme_mod('hero_image_subtitle') ) : ?>
            <span class="frontpage-site-description"><?php echo wp_kses_post(get_theme_mod('hero_image_subtitle')) ?></span>
          <?php endif; ?>
      </span>

    </div><!--.site-branding-->
  </div><!--.site-header-->
  <?php } else {  ?>

  <?php } ?>
</header>

<div id="content" class="site-content">
